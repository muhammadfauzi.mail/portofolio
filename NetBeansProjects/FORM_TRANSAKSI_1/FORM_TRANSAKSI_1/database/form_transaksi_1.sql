-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 22 Mar 2019 pada 12.01
-- Versi Server: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `form_transaksi_1`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `databarang`
--

CREATE TABLE `databarang` (
  `kodebarang` varchar(20) NOT NULL,
  `namabarang` varchar(20) NOT NULL,
  `stok` varchar(20) NOT NULL,
  `hargasatuan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `databarang`
--

INSERT INTO `databarang` (`kodebarang`, `namabarang`, `stok`, `hargasatuan`) VALUES
('KB001', 'KOPI', '60', '1500'),
('KB002', 'TEH', '70', '1500');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_transaksi`
--

CREATE TABLE `tabel_transaksi` (
  `kodetransaksi` varchar(50) NOT NULL,
  `tanggal` date NOT NULL,
  `kodebarang` varchar(20) NOT NULL,
  `namabarang` varchar(20) NOT NULL,
  `harga` int(50) NOT NULL,
  `jumlah` int(20) NOT NULL,
  `subtotal` int(50) NOT NULL,
  `total` int(20) NOT NULL,
  `bayar` int(20) NOT NULL,
  `kembalian` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tabel_transaksi`
--

INSERT INTO `tabel_transaksi` (`kodetransaksi`, `tanggal`, `kodebarang`, `namabarang`, `harga`, `jumlah`, `subtotal`, `total`, `bayar`, `kembalian`) VALUES
('KT001', '2019-03-22', 'KB001', 'KOPI', 1500, 2, 3000, 3000, 5000, 2000),
('KT002', '2019-03-22', 'KB002', 'TEH', 1500, 2, 3000, 3000, 5000, 2000),
('KT003', '2019-03-22', 'KB001', 'KOPI', 1500, 28, 42000, 42000, 50000, 8000),
('KT004', '2019-03-22', 'KB001', 'KOPI', 1500, 5, 7500, 7500, 10000, 2500),
('KT005', '2019-03-22', 'KB001', 'KOPI', 1500, 5, 7500, 7500, 10000, 2500),
('KT006', '2019-03-22', 'KB002', 'TEH', 1500, 28, 42000, 42000, 50000, 8000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `databarang`
--
ALTER TABLE `databarang`
  ADD PRIMARY KEY (`kodebarang`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
