/*
/; * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programputri1;
import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import javax.swing.*;


/**
 *
 * @author EX-Awesome
 */
public class Programputri1{
    

    /**
    * @param args the command line arguments
    t*/
    public static void main(String[] args) throws IOException {
        Scanner a = new Scanner(System.in);
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        double luas,z,volume;
        int l,t,x,p,alas;
        int pil;
        
        String bangun = "y";
        while (bangun.equals("y")){
            System.out.println("================================");
            System.out.println("===Mencari Rumus Bangun Ruang===");
            System.out.println("|| 1.Balok                    ||");
            System.out.println("|| 2.Kubus                    ||");
            System.out.println("|| 3.Exit                     ||");
            System.out.println("================================");
            System.out.println("Masukkan Pilihan Anda = ");
            pil = br.read();
            
            switch(pil){
                case '1':{
                    System.out.println("");
                    System.out.println("Mencari Hasil Luas & Volume Balok");
                    System.out.println("");
                    System.out.println("Masukkan Tinggi Balok  : ");
                    t = a.nextInt();
                    System.out.println("Masukkan Panjang Balok : ");
                    p = a.nextInt();
                    System.out.println("Masukkan Lebar Balok : ");
                    l = a.nextInt();
                    System.out.println("");
                    luas = (2*((p*l)+(p*t)+(t*l)));
                    System.out.println("Luas balok Adalah   = "+luas);
                    volume = (p*l*t);
                    System.out.println("Volume balok Adalah = "+volume);
                    int m=p;
                int n=0;
                int i1, i2;

                for (i1=0; i1<p-1; i1++) {
                    if (i1==0) {
                        for (i2=0; i2<m+1; i2++) {
                            System.out.print("   ");
                        }
                        for (i2=0; i2<p; i2++) {
                            System.out.print("  -");
                        }
                        System.out.println();
                    }
                }

                for (i1=m; i1>0; i1--) {
                    for (i2=i1; i2>0; i2--) {
                        System.out.print("   ");
                    }
                    System.out.print("  /");

                    for (i2=0; i2<p-1; i2++) {
                        System.out.print("   ");
                    }
                    System.out.print("  /");

                    if (i1!=m) {
                        for (i2=0; i2<m-i1; i2++) {
                            if (i1+i2==m-1) {
                                System.out.print("  |");
                                n++;
                            } 
                            else {
                                System.out.print("   ");
                            }
                        }
                    }
                    System.out.println();
                }

                i1=p-n;
                i2=0;

                for (int i=1; i<=p+2; i++) {
                    for (int j=1; j<=p; j++){
                        int i3;
                        if ((i==1) || (i==p+2)) {
                            System.out.print("  -");
                            if ((j==p) && (i==1)) {
                                for (i3=0; i3<m; i3++) {
                                System.out.print("   ");
                            }
                            System.out.print("  |");
                            }
                        }
                        else if ((j==1) || (j==p)) {
                            System.out.print("  |");
                            if ((j==p) && (i<=p+1))
                            if (i<=i1) {
                                for (i3=0; i3<m; i3++) {
                                    System.out.print("   ");
                                }
                                System.out.print("  |");
                            }
                            else{
                                for (i3=m-i2; i3>0; i3--) {
                                    System.out.print("   ");
                                }
                            i2++;
                            System.out.print("  /");
                            }
                        }
                        else{
                            System.out.print("   ");
                        }
                    }
                    System.out.println();
                }
                    System.out.println("Apakah anda ingin kembali?[Y/T] : ");
                    bangun = a.next();
                
                    break;
                }
                    
                case '2':{
                    System.out.println("");
                    System.out.println("Mencari Hasil Luas & Volume Kubus");
                    System.out.println("");
                    System.out.println("Masukkan Panjang Sisi : ");
                    x = a.nextInt();
                    System.out.println("Rumus Mencari Luas kubus 6 x sisi kuadrat");
                    luas = (2*(x*x));
                    System.out.flush();
                    System.out.println("");
                    System.out.println("Luas Kubus Adalah   = "+luas);
                    System.out.println("");
                    z = (x*x*x);
                    System.out.println("Volume Kubus Adalah = "+z);
                    int m=x/2;
                    int n=0;
                    int i1, i2;
                    for (i1=0; i1<x-1; i1++) {
                    if (i1==0) {
                        for (i2=0; i2<m+1; i2++) {
                            System.out.print("   ");
                        }
                        for (i2=0; i2<x; i2++) {
                            System.out.print("  -");
                        }
                        System.out.println();
                    }
                }

                for (i1=m; i1>0; i1--) {
                    for (i2=i1; i2>0; i2--) {
                        System.out.print("   ");
                    }
                    System.out.print("  /");

                    for (i2=0; i2<x-1; i2++) {
                        System.out.print("   ");
                    }
                    System.out.print("  /");

                    if (i1!=m) {
                        for (i2=0; i2<m-i1; i2++) {
                            if (i1+i2==m-1) {
                                System.out.print("  |");
                                n++;
                            } 
                            else {
                                System.out.print("   ");
                            }
                        }
                    }
                    System.out.println();
                }

                i1=x-n;
                i2=0;

                for (int i=1; i<=x+2; i++) {
                    for (int j=1; j<=x; j++){
                        int i3;
                        if ((i==1) || (i==x+2)) {
                            System.out.print("  -");
                            if ((j==x) && (i==1)) {
                                for (i3=0; i3<m; i3++) {
                                System.out.print("   ");
                            }
                            System.out.print("  |");
                            }
                        }
                        else if ((j==1) || (j==x)) {
                            System.out.print("  |");
                            if ((j==x) && (i<=x+1))
                            if (i<=i1) {
                                for (i3=0; i3<m; i3++) {
                                    System.out.print("   ");
                                }
                                System.out.print("  |");
                            }
                            else{
                                for (i3=m-i2; i3>0; i3--) {
                                    System.out.print("  ");
                                }
                            i2++;
                            System.out.print("  /");
                            }
                        }
                        else{
                            System.out.print("   ");
                        }
                    }
                    System.out.println();
                }
                    System.out.println("Apakah anda ingin kembali?[Y/T] : ");
                    bangun = a.next();
                    break;
                }
                case '3':{
                    System.out.println("Terima Kasih");
                    return;
                }
                default:{
                    System.out.println("Maaf Inputan Salah, Silahkan Diulang!!!");
                    break;
                }
            }
        }
        // TODO code application logic here
    }
    
}