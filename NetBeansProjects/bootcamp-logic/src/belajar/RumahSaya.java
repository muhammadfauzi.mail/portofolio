package belajar;

public class RumahSaya {
	
	public void doPublic() {
		System.out.println("Public Agent");
		
	}
	
	protected void doProtect() {
		System.out.println("Protect Agent");
		
	}
	
	private void doPrivate() {
		System.out.println("Private Agent");
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		RumahSaya rs = new RumahSaya();
		rs.doPublic();
		rs.doProtect();
		rs.doPrivate();
	}

}
