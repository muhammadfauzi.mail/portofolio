package Array2Dimensi;

import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import java.text.DecimalFormat;
 

public class Baru1 {

	public static void main(String[] args) {

		      JTextArea areaTampilan;
		      DecimalFormat duaAngka;
		      String tampilan;
		      double rata2;
		 
		      int[ ][ ] daftarNilai = {{50, 90, 60},
		                               {70, 50, 80},
		                               {80, 60, 70},
		                               {80, 80, 80},
		                               {60, 90, 70},
		                               {}};
		 
		      duaAngka = new DecimalFormat("0.00");
		      areaTampilan = new JTextArea();
		 
		      // Menampilkan nilai test, rata-rata dan kelulusan
		      tampilan = "KETERANGAN KELULUSAN HASIL TEST\n";
		      tampilan += "________________________________\n\n";
		      tampilan += "Pelamar\tDaftar Nilai\tRata-Rata\tKeterangan";
		 
		      for (int x = 0; x < daftarNilai.length; x++) {
		         tampilan += "\nPelamar" + x + "\t";
		         int jumlahNilai = 0;
		         for (int y = 0; y < daftarNilai[x].length; y++) {
		            tampilan += daftarNilai[x][y] + "  ";
		            jumlahNilai += daftarNilai[x][y];
		         }
		          
		         rata2 = (double) jumlahNilai / daftarNilai[x].length;
		         tampilan += "\t" + duaAngka.format(rata2);
		         if (rata2 >= 70.00)
		            tampilan += "\t" + "Lulus";
		         else
		            tampilan += "\t" + "Tidak lulus";
		      }
		       
		      areaTampilan.setText(tampilan);
		      JOptionPane.showMessageDialog(null, areaTampilan,
		         "Tabel Kelulusan", JOptionPane.INFORMATION_MESSAGE);
		 
		      // Mengakhiri aplikasi GUI
		      System.exit(0);
		   }
}

