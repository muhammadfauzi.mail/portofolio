package belajarArray;

public class ConvertVariable {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String huruf = "09";
		int angka = Integer.valueOf(huruf);
		System.out.println(huruf);
		System.out.println(angka);
		
		
		int limaInt = 5;
		String limaStr = String.valueOf(limaInt);
		System.out.println(limaStr + limaStr);
		
		double x = 4.13;
		String y = String.valueOf(x);
		System.out.println(y + y);
	}

}
