package looping;

public class LoopAngka {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a = 5;
		for (int i = 0; i < 5; i++) {
			System.out.printf("%2s",a);
		}
		System.out.println();
		//
		int t = 1;
		for (int i = 0; i < 5; i++) {
			System.out.printf("%2s",t);
			t = t + 2;
		}
		
		System.out.println();

		//
		int d = 5;
		for (int i = 0; i < 5; i++) {
			System.out.printf("%4s", d);
			d = d + 5;
			}
		System.out.println();
		
		int u = 20;
		for (int i = 0; i < 5; i++) {
			System.out.printf("%4s", u);
			u = u - 5;
		}
		System.out.println();
		
		int angka5 = 5;
		int angkaKe = 1;
		int cetak = 0;
		for (int i = 0; i < 10; i++) {
			if (angkaKe == 3) {
				cetak = angka5 * -1;
				angkaKe = 1;
			} else {
				cetak = angka5 * 1;
				angkaKe++;
			}
			System.out.printf("%5s", cetak);
			angka5 = angka5 + 5;
	}
		//
		System.out.println();
		
		int angka6 = 5;
		int angkaLagi = 1;
		int cetakLagi = 0;
		for (int i = 0; i < 10; i++) {
			if (angkaLagi == 3) {
				cetakLagi = angka6 * -1;
				angkaLagi = 1;
			}
			else if (angkaLagi == 2) {
				cetakLagi = 99;
				angkaLagi++;
				
			}
			 else {
				cetakLagi = angka6 * 1;
				angkaLagi++;
			}
			System.out.printf("%5s", cetakLagi);
			angka6 = angka6 + 5;
		}
	}
}