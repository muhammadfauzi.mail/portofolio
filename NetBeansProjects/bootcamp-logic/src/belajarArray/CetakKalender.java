package belajarArray;

public class CetakKalender {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String kalendar = "Senin 03/Feb/2019";
		
		
		
		String[] kalendarSplit = kalendar.split(" ");
		String hari = kalendarSplit[0];
		String tglLengkap = kalendarSplit[1];
		
		//output 03-2-2019
		String[] tglLengkapSplit = tglLengkap.split("/");
		String tgl = tglLengkapSplit[0];
		String bulan = tglLengkapSplit[1];
		String tahun = tglLengkapSplit[2];
		
		int bulanAngka = 0;
		String hariAngka = " ";
		KelasFungsi kf = new KelasFungsi();
		bulanAngka = kf.formatBulan(bulan);
		hariAngka = kf.formatHari(hari);
		
		String formatBaru = hariAngka+ " " +tgl + "-" +bulanAngka+ "-" + tahun;
		System.out.println(formatBaru);
	}

}
