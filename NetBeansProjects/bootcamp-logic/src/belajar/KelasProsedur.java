package belajar;

public class KelasProsedur {

	//public void namaMethod(Tipe varInput)
	
	public void methodKurang(Integer x, Integer y) {
		Integer z = x - y;
		System.out.println(z);
		
	}
	
	public void methodKali(Integer input1, Integer input2, String cetak) {
		Integer hasil = input1*input2;
		System.out.println(hasil);
		System.out.println(cetak);
		
	}
	
	public void methodHitung() { //method yang bersifat void adalah prosedur
		Integer a = 100;
		Integer b = 200;
		Integer c = a + b;
		System.out.println(c);
	}
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		KelasProsedur kp = new KelasProsedur(); // instance class
		kp.methodHitung();
		
		kp.methodKali(3, 5, "OGE");
		kp.methodKurang(100, 50);
		
		kelasFunction kf = new kelasFunction();
		Integer c = 0;
		c = kf.methodRumus1();
		System.out.println(c + 1000);
	
		String fullname = " ";
		fullname = kf.methodGabung("LALA", " LELE");
		System.out.println(fullname);
		
		Integer C = 0;
		C = kf.methodRumus2();
		System.out.println(C);

		KelasConstructor kc = new KelasConstructor(7);
		kc.methodRumus3();
	}


}
