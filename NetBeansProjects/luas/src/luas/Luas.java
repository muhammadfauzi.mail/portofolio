import java.awt.*;
import javax.swing.*;

public class DrawPolygons extends JFrame {

 public DrawPolygons() {
  super ("Menggambar Polygon");
  setSize (600,400);
  show();
 }

 public void paint(Graphics g) {
  super.paint (g);
  
  int xValues[] = {140,390,390,140};
  int yValues[] = {120,120,370,370};
  Polygon poly1 = new Polygon (xValues, yValues, 4); //(arrX, arrY, jumTitik)
  g.drawPolygon (poly1);
  
  int xValues2[] = {230,480,390,140};
  int yValues2[] = {40,40,120,120};
  Polygon poly2 = new Polygon (xValues2, yValues2, 4); //(arrX, arrY, jumTitik)
  g.drawPolygon (poly2);
  
  int xValues3[] = {230,480,480,230};
  int yValues3[] = {40,40,290,290};
  Polygon poly3 = new Polygon (xValues3, yValues3, 4); //(arrX, arrY, jumTitik)
  g.drawPolygon (poly3);
  
  int xValues4[] = {230,480,390,140};
  int yValues4[] = {290,290,370,370};
  Polygon poly4 = new Polygon (xValues4, yValues4, 4); //(arrX, arrY, jumTitik)
  g.drawPolygon (poly4);
  
  int xValues5[] = {390,480,480,390};
  int yValues5[] = {120,40,290,370};
  Polygon poly5 = new Polygon (xValues5, yValues5, 4); //(arrX, arrY, jumTitik)
  g.drawPolygon (poly5);
  //g.fillPolygon (poly5);

  int xValues6[] = {140,230,230,140};
  int yValues6[] = {120,40,290,370};
  Polygon poly6 = new Polygon (xValues6, yValues6, 4); //(arrX, arrY, jumTitik)
  g.drawPolygon (poly6);
  //g.fillPolygon (poly6);
  
  
 }

 public static void main (String args[]) {

  DrawPolygons test = new DrawPolygons();
  test.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 }

}