/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package percobaan;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 *
 * @author EX-Awesome
 */
public class Percobaan {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        Scanner a = new Scanner(System.in);
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        double luas,volume;
        int lebar,tinggi,sisi,panjang,alas;
        int pil;
        
        String bangun = "y";
        while (bangun.equals("y")){
            System.out.println("================================");
            System.out.println("===Mencari Rumus Bangun Ruang===");
            System.out.println("|| 1.Balok                    ||");
            System.out.println("|| 2.Kubus                    ||");
            System.out.println("|| 3.Exit                     ||");
            System.out.println("================================");
            System.out.println("Masukkan Pilihan Anda = ");
            pil = a.nextInt();A
            
            switch(pil){
                case 1:{
                    System.out.println("");
                    System.out.println("Mencari Hasil Luas & Volume Balok");
                    System.out.println("");
                    System.out.println("Masukkan Tinggi Balok  : ");
                    tinggi = a.nextInt();
                    System.out.println("Masukkan Panjang Balok : ");
                    panjang = a.nextInt();
                    System.out.println("Masukkan Lebar Balok : ");
                    lebar = a.nextInt();
                    System.out.println("");
                    luas = (2*((panjang*lebar)+(panjang*tinggi)+(tinggi*lebar)));
                    System.out.println("Luas balok Adalah   = "+luas);
                    volume = (panjang*lebar*tinggi);
                    System.out.println("Volume balok Adalah = "+volume);
                    System.out.println("Apakah anda ingin kembali?[Y/T] : ");
                    bangun = a.next();
                
                    break;
                }
                    
                case 2:{
                    System.out.println("");
                    System.out.println("Mencari Hasil Luas & Volume Kubus");
                    System.out.println("");
                    System.out.println("Masukkan Panjang Sisi : ");
                    sisi = a.nextInt();
                    System.out.println("Rumus Mencari Luas kubus 6 x sisi kuadrat");
                    luas = (2*(sisi*sisi));
                    System.out.flush();
                    System.out.println("");
                    System.out.println("Luas Kubus Adalah   = "+luas);
                    System.out.println("");
                    volume = (sisi*sisi*sisi);
                    System.out.println("Volume Kubus Adalah = "+volume);
                    System.out.println("Apakah anda ingin kembali?[Y/T] : ");
                    bangun = a.next();
                    break;
                }
                case 3:{
                    System.out.println("Terima Kasih");
                    return;
                }
                default:{
                    System.out.println("Pilhan Tidak Ada");
                    break;
                }
            }
        }
    }
    
}
