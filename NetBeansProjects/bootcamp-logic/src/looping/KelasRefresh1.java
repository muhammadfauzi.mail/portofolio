package looping;

import java.util.*;

public class KelasRefresh1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//1
		int angkaUmur = 22;
		
		for (int i = 0; i < angkaUmur; i++) {
			System.out.printf("%3s", angkaUmur);
		}
		System.out.println();
		
		//2
		
		int umur1 = 18;
		
		for (int i = 0; i < 11; i++) {
			System.out.printf("%4s", umur1);
			umur1 = umur1 + 10;
		}
		System.out.println();
		
		//3
		
		int umur2 = 18;
		for (int i = 0; i < 10; i++) {
			System.out.printf("%4s", umur2);
			umur2 = umur2 - 10;
		}
		System.out.println();
		
		//4
		
		int angka1 = 4;
		
		for (int i = 0; i < 10; i++) {
			System.out.printf("%4s", angka1);
			angka1 = angka1 + 4;
		}
		System.out.println();
		
		//5
		
		int angka2 = 4;
		int cetak = 0;
		int angkaKe = 1;
		for (int i = 0; i < 10; i++) {
			if (angkaKe == 3) {
				cetak = angka2 * 1;
				angkaKe = 1;
			}
			else if (angkaKe == 1) {
				cetak = angka2 * -1;
				angkaKe++;
			}
			else {
				cetak = angka2 * 1;
				angkaKe++;
			}
			System.out.printf("%4s", cetak);
			angka2 = angka2 + 4;
		}
		
		System.out.println();
		
		//6
		
		int angka3 = 4;
		int cetak1 = 0;
		
		for (int i = 0; i < 10; i++) {
			//System.out.printf("&4s", angka3);
			//angka3 = angka3 + 4;
			
			if (angka3 %8 == 0) {
				cetak1 = angka3 / 8;
			} else {
				cetak1 = angka3;
			}
			angka3 = angka3 + 4;
			System.out.printf("%4s", cetak1);
		}
		
		//7
		
		System.out.println();
		System.out.println();
		
		int angka4 = 4;
		for (int i = 0; i < 10; i++) {
			//System.out.printf("%4s", angka4);
			
			String stringangka4 = String.valueOf(angka4);
			if (stringangka4.contains("2") || stringangka4.contains("4")) {
				System.out.printf("%4s", 99);
			} else {
				System.out.printf("%4s", angka4);
			}
			angka4 = angka4 + 4;
		}
		
		System.out.println();
		
		//8
		
		int angka5 = 1;
		int n = 4;
		for (int i = 0; i < n; ++i) {
			for (int j = 1; j < angka5; j++) {
				System.out.print(angka5+ " ");
			}
			System.out.print(angka5+" ");
			angka5 = angka5 + 2;
		}
		System.out.println();
		System.out.println();
		
		//9
		
		int[] angka6 = {75, 1, 40, 2, 25, 8, 15, 3};
		 Arrays.sort(angka6);
		for (int i = 0; i < angka6.length; i++)
			System.out.println(angka6[i]+ "   ");
		for (int x = angka6.length -1; x >= 0; x--)
	        System.out.printf(angka6[x] + "   ");
		
		System.out.println();
		System.out.println();
		
		//10
		
		String[] hurup = {"P","I","L","O","T"};
		 Arrays.sort(hurup);
		for (int i = 0; i < hurup.length; i++)
			System.out.printf(hurup[i]+ "   ");
		System.out.println();
		System.out.println();
		
		//11
		
		String[] nasi = {"N", "A", "S", "I"};
		String[] asin = {"A", "S", "I", "W"};
		
			Arrays.sort(nasi);
			Arrays.sort(asin);
		
		String jawab1 = "";
		String jawab2 = "";
		
		for (int i = 0; i < nasi.length; i++) {
			System.out.printf(nasi[i]+ "  ");
			jawab1 = nasi[i];
		}
			System.out.println("");
		for (int j = 0; j < asin.length; j++) {
			System.out.printf(asin[j]+ "  ");
			jawab2 = asin[j];
		}
			System.out.println();
		
		if (jawab1 == jawab2) {
			  System.out.println("Anagram");
			} else {
			  System.out.println("Not Anagram");
			}
    
		//12
		
		
	
	
	
	}  
		
}

