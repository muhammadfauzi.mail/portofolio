package belajar;

public class PolyB extends PolyA { //polyB mempunyai fungsi juga, jadi meskipun kita memanggil polyA polyb lah yang akan muncul. 
	public void doTransportasi() {
		System.out.println("Jepang Pakai Kereta");
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PolyA pa = new PolyB();
		pa.doTransportasi();
		pa.doTambah(1, 3);		
		}

}
