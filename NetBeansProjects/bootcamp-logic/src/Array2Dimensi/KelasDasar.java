package Array2Dimensi;

public class KelasDasar {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int angka = 100;
		System.out.println(angka);
		System.out.println();
		
		int[] nilai = new int[3];
		nilai[0] = 100;
		nilai[1] = 200;
		nilai[2] = 300;
		for (int i = 0; i < nilai.length; i++) {
			System.out.println(nilai[i]);
		}
		
		int[][] pola = new int[3][2];
		pola[0][0] = 100;
		pola[0][1] = 200;
		
		pola[1][0] = 300;
		pola[1][1] = 400;
		
		pola[2][0] = 500;
		pola[2][1] =600;
		System.out.println();
		
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 2; j++) {
				System.out.printf("%4s", pola[i][j]);
			}
			System.out.println();
			
			System.out.println();
			
		}
		
		
	}

}
